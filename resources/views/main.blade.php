@extends('app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          Input the test case
        </div>
        <div class="panel-body">
          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{!! $error !!}</li>
              @endforeach
            </ul>
          </div>
          @endif
          <div class="row">
            <div class="col-sm-6">
              {!! Form::open(array('url' => 'main/process', 'method' => 'get')) !!}
              <div class="form-group">
                {!! Form::textarea('input','',array('placeholder' => '')) !!}            
              </div>
              <div class="form-group">
                {!! Form::submit('Do it',array('class' => 'btn btn-default')) !!}
              </div>
              {!! Form::close() !!}              
            </div>
            <div class="col-sm-6">
              @if (isset($result))
              <h2>Output</h2>
              <ul>
                @foreach ($result as $line)
                <li>{{ $line }}</li>
                @endforeach                
              </ul>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
