<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cube\Providers;

use \Illuminate\Support\ServiceProvider;
use Cube\Validators\CubeInputValidator;
/**
 * Description of CubeInputValidatorServiceProvider
 *
 * @author Personal
 */
class CubeInputValidatorServiceProvider extends ServiceProvider
{
  //put your code herepublic function register(){}
 
  public function boot()
  {
    $this->app->validator->resolver(function($translator, $data, $rules, $messages)
    {
      return new CubeInputValidator($translator, $data, $rules, $messages);
    });
  }

  public function register()
  {
    
  }

}
