<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cube\Validators;

use Illuminate\Validation\Validator;

/**
 * Description of CubeInputValidator
 *
 * @author Personal
 */
class CubeInputValidator extends Validator
{

    public function validateCubeInputFormat($attribute, $value, $parameters)
    {
        $t = isset($parameters[0]) ? (int) $parameters[0] : 50;
        $n = isset($parameters[1]) ? (int) $parameters[1] : 100;
        $m = isset($parameters[2]) ? (int) $parameters[2] : 1000;

        $text = explode(PHP_EOL, $value);
        foreach ($text as &$line)
        {
            $line = trim($line);
        }

        if (!is_integer($text[0]) && (intval($text[0]) < 0 || intval($text[0]) > $t))
        {
            return FALSE;
        }

        $test_cases = intval($text[0]);

        if ($test_cases > $t || $test_cases < 0)
        {
            return FALSE;
        }

        array_shift($text);
        for ($i = 0; $i < $test_cases; $i++)
        {
            list($matrix, $operations) = explode(' ', $text[0]);

            if (intval($matrix) > $n || intval($matrix) < 0)
            {
                return FALSE;
            }

            if (intval($operations) > $m || intval($operations) < 0)
            {
                return FALSE;
            }

            array_shift($text);
            for ($j = 0; $j < $operations; $j++)
            {
                list($method, $command) = explode(' ', $text[$j], 2);
                $command = explode(' ', $command);
                if (empty($method) && (strtolower($method) != 'update' || strtolower($method) != 'query'))
                {
                    return FALSE;
                }

                if (strtolower($method) == 'update')
                {
                    if (count($command) != 4)
                    {
                        return FALSE;
                    }
                }
                

                if (strtolower($method) == 'query')
                {                    
                    if (count($command) != 6)
                    {
                        return FALSE;
                    }
                }
            }
            array_splice($text, 0, $j);
        }

        return TRUE;
    }

}
