<?php

namespace Cube\Http\Requests;

use Cube\Http\Requests\Request;

class InputCubeRequest extends Request
{

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'input' => 'cube_input_format:50,100,1000'
    ];
  }

}
