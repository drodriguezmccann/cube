<?php

namespace Cube\Http\Controllers;

use Cube\Http\Requests\InputCubeRequest;
use Cube\Utils\InputParserCube;

class MainController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Main controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
   */

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    
  }

  /**
   * Show the application welcome screen to the user.
   *
   * @return Response
   */
  public function index()
  {
    return view('main')->with(\Request::old());
  }

  public function process(InputCubeRequest $request)
  {
    $data = \Request::get('input');
    $parser = new InputParserCube($data);

    return \Redirect::to('/')->withInput(array('result' => $parser->executeInput()));
  }

}
