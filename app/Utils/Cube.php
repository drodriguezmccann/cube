<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cube\Utils;

/**
 * Description of Cube
 *
 * @author Personal
 */
class Cube
{

  private $blocks;

  public function __construct($n)
  {
    $this->blocks = [];

    for ($x = 1; $x <= $n; $x++)
    {
      $this->blocks[$x] = [];
      for ($y = 1; $y <= $n; $y++)
      {
        $this->blocks[$x][$y] = [];
        for ($z = 1; $z <= $n; $z++)
        {
          $this->blocks[$x][$y][$z] = 0;
        }
      }
    }
  }

  public function update($command)
  {
    $x = $command[0];
    $y = $command[1];
    $z = $command[2];
    $value = $command[3];
    $this->blocks[$x][$y][$z] = intval($value);
    return NULL;
  }

  public function query($command)
  {
    $x1 = intval($command[0]);
    $y1 = intval($command[1]);
    $z1 = intval($command[2]);
    $x2 = intval($command[3]);
    $y2 = intval($command[4]);
    $z2 = intval($command[5]);
    $sum = 0;
    for ($x = $x1; $x <= $x2; $x++)
    {
      for ($y = $y1; $y <= $y2; $y++)
      {
        for ($z = $z1; $z <= $z2; $z++)
        {
          $sum += $this->blocks[$x][$y][$z];
        }
      }
    }
    
    return $sum;
  }

  public function execute($operations)
  {
    $output =[];
    foreach ($operations as $key => $operation)
    {
      list($method, $command) = explode(' ', $operation, 2);
      $command = explode(' ', $command);
      $result = $this->$method($command);
      if($result !== NULL){
        $output[] = $result;        
      }
    }
    return $output;
  }

  public function getBlocks()
  {
    return $this->blocks;
  }

}
