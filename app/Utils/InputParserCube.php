<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cube\Utils;

use Cube\Utils\Cube;

/**
 * Description of InputParserCube
 *
 * @author Personal
 */
class InputParserCube
{

    private $input;
    private $test_cases;
    private $matrices;

    public function __construct($input)
    {
        $this->input = explode(PHP_EOL, $input);
        $this->test_cases = [];
        $this->matrices = [];
        $num_test_cases = intval($this->input[0]);
        array_shift($this->input);
        for ($i = 0; $i < $num_test_cases; $i++)
        {
            list($this->matrices[$i], $num_operations) = explode(' ', $this->input[0]);
            array_shift($this->input);
            for ($k = 0; $k < $num_operations; $k++)
            {
                $this->test_cases[$i][$k] = strtolower($this->input[$k]);
            }
            array_splice($this->input, 0, $k);
        }
    }

    public function executeInput()
    {
        $output = [];

        $num_test_cases = count($this->test_cases);

        for ($i = 0; $i < $num_test_cases; $i++)
        {
            $cube = new Cube($this->matrices[$i]);
            $output = array_merge($output, $cube->execute($this->test_cases[$i]));
        }
        return $output;
    }

}
